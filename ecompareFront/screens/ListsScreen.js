import React from "react";
import { AsyncStorage, ScrollView, StyleSheet, Text, View } from "react-native";
import { ListItem, Icon } from "react-native-elements";
import { ExpoLinksView } from "@expo/samples";
import list from "../assets/MockData/Lists.json";
import axios from "axios";
import Endpoints from "../constants/Endpoints";

export default class LinksScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = { list: [] };
  }

  async componentDidMount() {
    var self = this;
    axios
      .get(Endpoints.ShoppingListGet)
      .then(function(response) {
        console.log(response.data);
        self.setState({ list: response.data });
      })
      .catch(function(error) {
        console.log(error);
      });
    /*var obj = {};
    var settings = await AsyncStorage.getItem("list");
    if (settings == null) {
      AsyncStorage.getItem("list").then(function(strResult) {
        var result = JSON.parse(strResult) || {};
        result = list.slice();
        AsyncStorage.setItem("list", JSON.stringify(result));
      });
    }
    var listTemp = [];
    var settings = await AsyncStorage.getItem("list");
    listTemp = JSON.parse(settings);
    this.setState({ list: listTemp });*/
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: "Listas de compra",
      headerRight: (
        <View>
          <Icon
            name="add-shopping-cart"
            color="white"
            onPress={() => navigation.navigate("AddList")}
          />
        </View>
      ),
      headerStyle: {
        backgroundColor: "#282b2e"
      },
      headerTintColor: "#fff"
    };
  };

  render() {
    return (
      <View>
        <ScrollView>
          {this.state.list &&
            this.state.list.map((l, i) => (
              <ListItem
                key={i}
                roundAvatar
                avatar={{
                  uri:
                    "https://image.freepik.com/free-icon/shopping-paper-bag-outline_318-39786.jpg"
                }}
                title={l.name}
                subtitle={l.products.length > 0 ? l.products.map(function(item) {
                  return item['name'];
                }).toString() : "Lista de productos"}
              />
            ))}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: "#fff"
  }
});
