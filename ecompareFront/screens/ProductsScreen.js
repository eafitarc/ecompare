import React from "react";
import { AsyncStorage, ScrollView, StyleSheet, Text, View } from "react-native";
import { ListItem, Icon } from "react-native-elements";

import products from "../assets/MockData/products";

export default class SettingsScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: "Comparación Productos",
      headerStyle: {
        backgroundColor: "#282b2e"
      },
      headerRight: (
        <View>
          <Icon
            name="add"
            color="white"
            onPress={() => navigation.navigate("AddProduct")}
          />
        </View>
      ),
      headerTintColor: "#fff"
    };
  };

  constructor(props) {
    super(props);
    this.state = { products: [] };
    this._navigateToProduct = this._navigateToProduct.bind(this);
  }

  async componentDidMount() {
    var obj = {};
    var settings = await AsyncStorage.getItem("products");
    if (settings == null) {
      AsyncStorage.getItem("products").then(function(strResult) {
        var result = JSON.parse(strResult) || {};
        result = products.slice();
        AsyncStorage.setItem("products", JSON.stringify(result));
      });
    }
    var listTemp = [];
    var settings = await AsyncStorage.getItem("products");
    listTemp = JSON.parse(settings);
    this.setState({ products: listTemp });
  }

  _navigateToProduct(productId, productName) {
    this.props.navigation.navigate("Product", {
      productId,
      productName
    });
  }

  render() {
    return (
      <View>
        <ScrollView>
          {this.state.products &&
            this.state.products.map((l, i) => (
              <ListItem
                key={l.id || l.name}
                avatar={{ uri: l.image }}
                title={l.name}
                roundAvatar
                subtitle={l.category}
                onPress={() => this._navigateToProduct(l.id, l.name)}
              />
            ))}
        </ScrollView>
      </View>
    );
  }
}
