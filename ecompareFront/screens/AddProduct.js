import React from "react";
import {
  AsyncStorage,
  Button,
  Image,
  ScrollView,
  StyleSheet,
  TextInput,
  View
} from "react-native";

import { NavigationActions, StackActions } from 'react-navigation';
import { Icon } from "react-native-elements";

import products from "../assets/MockData/products.json";

export default class LinksScreen extends React.Component {
  constructor(props) {
    super(props);
    this.Adicionar = this.Adicionar.bind(this);
    this.state = { name: "", category: "", brand: "", price: "", url: "" };
  }

  Adicionar() {
    const { name, category, brand, price, url } = this.state;
    
    AsyncStorage.getItem('products').then(function(strResult) {
      var result = JSON.parse(strResult) || {};
      var maxObj = result.reduce(function(max, obj) {
        return obj.id > max.id? obj : max;
      });   
      var maxNumber = maxObj.id +1;
      result.push({id: maxNumber,name,category,brand,price,url});
      AsyncStorage.setItem('products', JSON.stringify(result));
    });
    const { navigate } = this.props.navigation;
    const resetAction = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: 'Home'})
      ],
      key: null // THIS LINE
    });
    this.props.navigation.dispatch(resetAction);
  }

  static navigationOptions = {
    headerTitle: "Agregar nuevo producto",
    headerStyle: {
      backgroundColor: "#282b2e"
    },
    headerTintColor: "#fff"
  };

  render() {
    return (
      <View style={styles.container}>
        <ScrollView
          style={styles.container}
          contentContainerStyle={styles.contentContainer}
        >
          <Image
            source={require("../assets/images/price-tag.jpg")}
            style={styles.imagePrice}
          />
          <View style={styles.textSection}>
            <Icon name="book" style={styles.textIcon} size={20} />
            <TextInput
              placeholder="Nombre"
              style={styles.input}
              onChangeText={name => this.setState({ name })}
            />
          </View>
          <View style={styles.textSection}>
            <Icon name="tag-faces" style={styles.textIcon} size={20} />
            <TextInput
              placeholder="Categoria"
              style={styles.input}
              onChangeText={category => this.setState({ category })}
            />
          </View>
          <View style={styles.textSection}>
            <Icon name="tag-faces" style={styles.textIcon} size={20} />
            <TextInput
              placeholder="Proveedor"
              style={styles.input}
              onChangeText={brand => this.setState({ brand })}
            />
          </View>
          <View style={styles.textSection}>
            <Icon name="tag-faces" style={styles.textIcon} size={20} />
            <TextInput
              placeholder="Precio"
              style={styles.input}
              onChangeText={price => this.setState({ price })}
            />
          </View>
          <View style={styles.textSection}>
            <Icon name="tag-faces" style={styles.textIcon} size={20} />
            <TextInput
              placeholder="Url"
              style={styles.input}
              onChangeText={url => this.setState({ url })}
            />
          </View>
          <View style={{ margin: 7 }} />
          <Button
            onPress={() => this.Adicionar()}
            title="Adicionar"
            color="#91130B"
            borderColor="transparent"
            style={styles.buttonStyle}
          />
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    padding: 12
  },
  contentContainer: {
    paddingTop: 0
  },
  imagePrice: {
    alignItems: "center",
    marginHorizontal: 100,
    width: 100,
    height: 100
  },
  titleStyle: {
    alignItems: "center",
    fontWeight: "700",
    fontSize: 27,
    padding: 12,
    textAlign: "center"
  },
  buttonStyle: {
    backgroundColor: "#282b2e",
    width: 300,
    height: 45,
    borderColor: "transparent",
    borderWidth: 0
  },
  containerStyle: { marginTop: 20 },
  textStyle: {
    padding: 12,
    paddingHorizontal: 10
  },
  textSection: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff"
  },
  textIcon: {
    padding: 10
  },
  input: {
    flex: 1,
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 0,
    backgroundColor: "#fff",
    color: "#424242"
  }
});
