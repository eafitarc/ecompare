import React from "react";
import {
  AsyncStorage,
  Button,
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from "react-native";
import { WebBrowser } from "expo";
import { NavigationActions, StackActions } from 'react-navigation';

import { MonoText } from "../components/StyledText";

import list from '../assets/MockData/Lists.json';
import products from '../assets/MockData/products.json';

export default class HomeScreen extends React.Component {
  static navigationOptions = {
    headerTitle: "eCompare",
    headerStyle: {
      backgroundColor: "#282b2e"
    },
    headerTintColor: '#fff',
  };

  async Reset(){
    AsyncStorage.setItem('list', JSON.stringify(list));
    AsyncStorage.setItem('products', JSON.stringify(products));
    const { navigate } = this.props.navigation;
    const resetAction = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: 'Home'})
      ],
      key: null // THIS LINE
    });
    this.props.navigation.dispatch(resetAction);
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView
          style={styles.container}
          contentContainerStyle={styles.contentContainer}
        >
          <Image
            source={require("../assets/images/Untitled.png")}
            style={styles.imagePrice}
          />
          <Button
            onPress={() => this.Reset()}
            title="Reset"
            color="#91130B"
            borderColor="transparent"
            style={styles.buttonStyle}
          />

          <Text style={styles.getStartedText}>
            Bienvenido a eCompare la aplicación de comparación de precios!
          </Text>
          <Text />
          <Text style={styles.getStartedText}>
            Toca el botón de productos para comenzar a comparar o
            la lista de compras para administrar!
          </Text>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    padding: 12
  },
  imagePrice: {
    alignItems: "center",
    marginHorizontal: 60
  },
  contentContainer: {
    paddingTop: 30
  },
  getStartedContainer: {
    alignItems: "center",
    marginHorizontal: 50
  },
  getStartedText: {
    fontSize: 17,
    color: "rgba(96,100,109, 1)",
    lineHeight: 24,
    textAlign: "center"
  }
});
