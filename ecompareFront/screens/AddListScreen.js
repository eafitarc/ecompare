import React from "react";
import { AsyncStorage, Button, Image, ScrollView, StyleSheet, TextInput, View } from "react-native";
import { Icon } from "react-native-elements";
import { NavigationActions, StackActions } from 'react-navigation';

import list from '../assets/MockData/Lists.json';
import axios from "axios";
import Endpoints from "../constants/Endpoints";

export default class LinksScreen extends React.Component {

  constructor(props) {
    super(props);
    this.Adicionar = this.Adicionar.bind(this);
    this.state = {name: '', description: ''};
  }

  Adicionar(){
    const {name, description} = this.state;
    axios.post(Endpoints.ShoppingListCreate, {
      name: name
    })
    .then(function (response) {
      console.log(response);
    })
    .catch(function (error) {
      console.log(error);
    });
    /*AsyncStorage.getItem('list').then(function(strResult) {
      var result = JSON.parse(strResult) || {};   
      result.push({name,description});
      AsyncStorage.setItem('list', JSON.stringify(result));
    });*/
    const { navigate } = this.props.navigation;
    const resetAction = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: 'Home'})
      ],
      key: null // THIS LINE
    });
    this.props.navigation.dispatch(resetAction);
  }

  static navigationOptions = {
    headerTitle: "Agregar nueva lista",
    headerStyle: {
      backgroundColor: "#282b2e"
    },
    headerTintColor: "#fff"
  };

  render() {
    return (
      <View style={styles.container}>
        <ScrollView
          style={styles.container}
          contentContainerStyle={styles.contentContainer}
        >
          <Image
            source={require("../assets/images/shopping.png")}
            style={styles.imagePrice}
          />
          <View style={styles.textSection}>
            <Icon name="book" style={styles.textIcon} size={20} />
            <TextInput placeholder="Nombre" style={styles.input} onChangeText={(name) => this.setState({name})} />
          </View>
          <View style={styles.textSection}>
            <Icon name="tag-faces" style={styles.textIcon} size={20} />
            <TextInput placeholder="Descripcion" style={styles.input} onChangeText={(description) => this.setState({description})} />
          </View>
          <View style={{ margin: 7 }} />
          <Button
            onPress={() => this.Adicionar()}
            title="Adicionar"
            color="#91130B"
            borderColor="transparent"
            style={styles.buttonStyle}
          />
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    padding: 12
  },
  contentContainer: {
    paddingTop: 0
  },
  imagePrice: {
    alignItems: "center",
    marginHorizontal: 50
  },
  titleStyle: {
    alignItems: "center",
    fontWeight: "700",
    fontSize: 27,
    padding: 12,
    textAlign: "center"
  },
  buttonStyle: {
    backgroundColor: "#282b2e",
    width: 300,
    height: 45,
    borderColor: "transparent",
    borderWidth: 0
  },
  containerStyle: { marginTop: 20 },
  textStyle: {
    padding: 12,
    paddingHorizontal: 10
  },
  textSection: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff"
  },
  textIcon: {
    padding: 10
  },
  input: {
    flex: 1,
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 0,
    backgroundColor: "#fff",
    color: "#424242"
  }
});
