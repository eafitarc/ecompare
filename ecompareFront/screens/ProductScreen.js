import React from "react";
import {
  AsyncStorage,
  Button,
  Dimensions,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  View
} from "react-native";
import { ListItem, Icon } from "react-native-elements";
export default class SettingsScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {product: {}};
    this.CompararProducto = this.CompararProducto.bind(this);
  }

  static navigationOptions = ({ navigation }) => {
    const productName = navigation.getParam("productName", "NO-NAME");
    return {
      headerTitle: `Producto: ${productName}`,
      headerStyle: {
        backgroundColor: "#282b2e"
      },
      headerTintColor: "#fff"
    };
  };

  async componentDidMount(){
    var obj = {};
    var settings = await AsyncStorage.getItem('products');
    if (settings == null){
      AsyncStorage.getItem('products').then(function(strResult) {
        var result = JSON.parse(strResult) || {};
        result = product.slice();
        AsyncStorage.setItem('products', JSON.stringify(result));
        
      });
    }
    var listTemp = [];
    var settings =  await AsyncStorage.getItem('products');
    listTemp = JSON.parse(settings);
    const itemId = parseInt(
      this.props.navigation.getParam("productId", "0"),
      10
    );
    this.setState({product: listTemp.find(x => x.id === itemId)});
  }

  CompararProducto(){
    const itemId = parseInt(
      this.props.navigation.getParam("productId", "0"),
      10
    );
    this.props.navigation.navigate("Compare", {
      itemId
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <ScrollView
          style={styles.container}
          contentContainerStyle={styles.contentContainer}
        >
          <Image source={{ uri: `${this.state.product.image}` }} style={styles.imagePrice} />
          <View style={styles.productInfo}>
            <View style={styles.leftContainer}>
              <Text style={styles.normalText}>{this.state.product.name}</Text>
            </View>
            <View style={styles.rightContainer}>
              <Text style={styles.italicText}>{this.state.product.price}</Text>
            </View>
          </View>
          <Text style={styles.titles}>Descripcion</Text>
          <Text style={styles.normalText}>{this.state.product.category}</Text>
          <Text style={styles.titles}>Proveedor</Text>
          <Text style={styles.normalText}>{this.state.product.brand}</Text>
          <Button
            onPress={() => this.CompararProducto()}
            title="Comparar"
            color="#91130B"
            borderColor="transparent"
            style={styles.buttonStyle}
          />
        </ScrollView>
      </View>
    );


  }
}

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  imagePrice: {
    alignItems: "center",
    width,
    height: 200
  },
  contentContainer: {
    paddingTop: 30
  },
  leftContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "flex-start"
  },
  rightContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "flex-end"
  },
  productInfo: {
    flex: 1,
    flexDirection: "row"
  },
  normalText: {
    fontSize: 20,
    padding: 18
  },
  italicText: {
    fontSize: 20,
    fontStyle: "italic",
    padding: 18
  },
  titles: {
    fontWeight: "bold",
    fontSize: 24,
    paddingLeft: 18,
    paddingRight: 18
  },
  buttonStyle: {
    backgroundColor: "#91130B",
    width: 300,
    height: 45,
    borderColor: "transparent",
    borderWidth: 0,
    padding: 18
  }
});
