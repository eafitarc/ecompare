import React, { Component } from "react";
import {
  Button,
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import { Icon } from "react-native-elements";

export default class Login extends Component {
  static navigationOptions = {
    title: "eCompare - Please log in"
  };

  render() {
    return (
      <View style={styles.container}>
        <ScrollView
          style={styles.container}
          contentContainerStyle={styles.contentContainer}
        >
          <Image
            source={require("../assets/images/Untitled.png")}
            style={styles.imagePrice}
          />
          <Text style={styles.titleStyle}>eCompare</Text>
          <View style={styles.textSection}>
            <Icon name="mail" style={styles.textIcon} size={20} />
            <TextInput placeholder="Email" style={styles.input} />
          </View>
          <View style={styles.textSection}>
            <Icon name="lock" style={styles.textIcon} size={20} />
            <TextInput placeholder="Password" style={styles.input} secureTextEntry={true} />
          </View>
          <View style={{ margin: 7 }} />
          <Button
            onPress={this._signInAsync}
            title="Login"
            color="#282b2e"
            borderColor="transparent"
            style={styles.buttonStyle}
          />
        </ScrollView>
      </View>
    );
  }

  _signInAsync = async () => {
    this.props.navigation.navigate("App");
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    padding: 12
  },
  contentContainer: {
    paddingTop: 0
  },
  imagePrice: {
    alignItems: "center",
    marginHorizontal: 50
  },
  titleStyle: {
    alignItems: "center",
    fontWeight: "700",
    fontSize: 27,
    padding: 12,
    textAlign: "center"
  },
  buttonStyle: {
    backgroundColor: "#282b2e",
    width: 300,
    height: 45,
    borderColor: "transparent",
    borderWidth: 0
  },
  containerStyle: { marginTop: 20 },
  textStyle: {
    padding: 12,
    paddingHorizontal: 10
  },
  textSection: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff"
  },
  textIcon: {
    padding: 10
  },
  input: {
    flex: 1,
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 0,
    backgroundColor: "#fff",
    color: "#424242"
  }
});
