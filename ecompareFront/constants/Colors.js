const Colors = {
    tabIconSelected : "#91130B",
    tabIconDefault: "#282b2e"
}

export default Colors;