import React from 'react';
import { createSwitchNavigator, createStackNavigator } from 'react-navigation';

import HomeScreen from '../navigation/MainTabNavigator';
import LoginScreen from '../screens/LoginScreen';

const AppStack = createStackNavigator({ Home: HomeScreen });
const AuthStack = createStackNavigator({ SignIn: LoginScreen })

export default createSwitchNavigator(
  {
  // You could add another route here for authentication.
  // Read more at https://reactnavigation.org/docs/en/auth-flow.html
    App: AppStack,
    Auth: AuthStack
  },
  {
    initialRouteName: 'Auth',
  }
);