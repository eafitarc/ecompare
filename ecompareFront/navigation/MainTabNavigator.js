import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';

import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import ListsScreen from '../screens/ListsScreen';
import ProductsScreen from '../screens/ProductsScreen';
import ProductScreen from '../screens/ProductScreen';
import CompareScreen from '../screens/CompareScreen';
import AddListScreen from '../screens/AddListScreen';
import AddProduct from '../screens/AddProduct';

const ProductStack = createStackNavigator({ Product: ProductScreen });
const CompareStack = createStackNavigator({ Compare: CompareScreen });
const AddListStack = createStackNavigator({ AddList: AddListScreen });
const AddProductStack = createStackNavigator({ AddList: AddProduct });

const HomeStack = createStackNavigator({
  Home: HomeScreen,
});

HomeStack.navigationOptions = {
  title: 'eCompare - Please sign in',
  tabBarLabel: 'Home',
  header: null,
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      title='Home'
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-information-circle${focused ? '' : '-outline'}`
          : 'md-information-circle'
      }
    />
  ),
};

const ProductsStack = createStackNavigator({
  Products: ProductsScreen,
  Product: ProductScreen,
  Compare: CompareScreen,
  AddProduct: AddProduct
});

ProductsStack.navigationOptions = {
  title: 'eCompare - Please sign in',
  tabBarLabel: 'Productos',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      title='Productos'
      focused={focused}
      name={Platform.OS === 'ios' ? `ios-pricetag${focused ? '' : '-outline'}` : 'md-pricetag'}
    />
  ),
};

const ListsStack = createStackNavigator({
  Lists: ListsScreen,
  AddList: AddListScreen
});

ListsStack.navigationOptions = {
  tabBarLabel: 'Listas',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      title='Listas'
      focused={focused}
      name={Platform.OS === 'ios' ? `ios-list${focused ? '' : '-outline'}` : 'md-list'}
    />
  ),
};

const bottomTabNavigator = createBottomTabNavigator(
{
  HomeStack,
  ProductsStack,
  ListsStack,
},{
  labelTintColor: '#e91e63',
  labelStyle: {
    fontSize: 12,
    color:'black',
  },

});

bottomTabNavigator.navigationOptions = ({ navigation }) => {
  // You can do whatever you like here to pick the title based on the route name
  const header = null;

  return {
    header,
  };
};

export default bottomTabNavigator;
