var model = require('../models/product');
var Product = model.Product;
var ShopList = model.ShopList;
var SuperMarket = model.SuperMarket;

//Simple version, without validation or sanitation
exports.test = function (req, res) {
    res.send('Greetings from the Test controller!');
};

exports.product_get_all = function (req, res, next) {
    Product.find(function (err, product_list) {
        if (err) return next(err);
        res.send(product_list);
    })
};

exports.get_list_shop = function (req, res, next) {
    ShopList.find(function (err, product_list) {
        if (err) return next(err);
        res.send(product_list);
    })
};

exports.product_create = function (req, res, next) {
    var product = new Product(
        {
            name: req.body.name,
            price: req.body.price,
            supermarket: req.body.supermarket,
            category: req.body.category,
            brand: req.body.brand,
            image: req.body.image
        }
    );
   console.log(req.body.name);
    product.save(function (err) {
        if (err) {
            return next(err);
        }
        res.send('Product Created successfully')
    })
};

exports.create_super = function (req, res, next) {
    var super_market = new SuperMarket(
        {
            id: req.body.id,
            name: req.body.name,
            image: req.body.image
        }
    );
    super_market.save(function (err) {
        if (err) {
            return next(err);
        }
        res.send('Supermarket Created successfully')
    })
};

exports.get_list_super = function (req, res, next) {
    SuperMarket.find(function (err, super_list) {
        if (err) return next(err);
        res.send(super_list);
    })
};

exports.create_shop_list = function (req, res,next) {
    var shop_list = new ShopList(
        {
            name: req.body.name,
            products: req.body.products
        }
    );

    shop_list.save(function (err) {
        if (err) {
            return next(err);
        }
        res.send('Shop List Created successfully')
    })
};

exports.product_details = function (req, res, next) {
    Product.findById(req.params.id, function (err, product) {
        if (err) return next(err);
        res.send(product);
    })
};

exports.product_update = function (req, res, next) {
    Product.findByIdAndUpdate(req.params.id, {$set: req.body}, function (err, product) {
        if (err) return next(err);
        res.send('Product udpated.');
    });
};

exports.product_delete = function (req, res, next) {
    Product.findByIdAndRemove(req.params.id, function (err) {
        if (err) return next(err);
        res.send('Deleted successfully!');
    })
};