var express = require('express');
var router = express.Router();

// Require the controllers
var product_controller = require('../controllers/product');


// a simple test url to check that all of our files are communicating correctly.
router.get('/test', product_controller.test);

router.get('/get/all', product_controller.product_get_all);

router.get('/get/list/super', product_controller.get_list_super);

router.get('/get/list/shop', product_controller.get_list_shop);

router.post('/create', product_controller.product_create);

router.post('/create/super', product_controller.create_super);

router.post('/create/shop/list', product_controller.create_shop_list);

router.get('/:id', product_controller.product_details);

router.put('/:id/update', product_controller.product_update);

router.delete('/:id/delete', product_controller.product_delete);


module.exports = router;